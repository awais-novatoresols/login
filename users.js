const express = require("express");
const router = express.Router();
const bcrypt = require("jsonwebtoken");
const keys = require("./keys");

//Load input validation

const validateRegisterInput = require("./register");
const validateloginInput = require("./login");
//Load user model

const Usre = require("./User");
